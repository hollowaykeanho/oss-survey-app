class CreateResponses < ActiveRecord::Migration
  def change
    create_table :responses do |t|
      t.string :location
      t.integer :age
      t.integer :tech_interest
      t.integer :tech_level
      # ta stands for technical_aspects
      t.integer :ta_web_app
      t.integer :ta_mobile_app
      t.integer :ta_static_web
      t.integer :ta_search_engine
      t.integer :ta_online_games
      t.text :ta_others
      # tp stands for technical_preferences
      t.integer :tp_online_courses
      t.integer :tp_edu_games
      t.integer :tp_video_tutorial
      t.integer :tp_coding_bootcamp
      t.integer :tp_day_workshop
      t.text :tp_other
      # ts stands for technical_stopper
      t.integer :ts_expensive
      t.integer :ts_no_time
      t.integer :ts_lack_support
      t.integer :ts_low_priority
      t.text :ts_other
      # rt stands for rewards type
      t.integer :reward_motive
      t.integer :rt_beauty_fashion
      t.integer :rt_food
      t.integer :rt_travel
      t.integer :rt_edu_scholarship
      t.integer :rt_job
      t.text :rt_other
      # wn stands for why no
      t.integer :wn_dry_and_boring
      t.integer :wn_too_difficult
      t.integer :wn_not_a_girl_thing
      t.integer :wn_just_not_for_me
      t.string :public_key

      t.timestamps null: false
    end
  end
end
