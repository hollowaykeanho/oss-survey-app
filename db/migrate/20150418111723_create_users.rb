class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :public_key
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :location
      t.integer :mailchimp_status
      t.integer :response_id

      t.timestamps null: false
    end
  end
end
