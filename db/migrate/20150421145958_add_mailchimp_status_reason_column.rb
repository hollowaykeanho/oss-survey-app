class AddMailchimpStatusReasonColumn < ActiveRecord::Migration
  def change
    add_column :users, :mailchimp_status_reason, :string, after: :mailchimp_status
  end
end
