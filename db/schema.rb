# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150421145958) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "responses", force: :cascade do |t|
    t.string   "location"
    t.integer  "age"
    t.integer  "tech_interest"
    t.integer  "tech_level"
    t.integer  "ta_web_app"
    t.integer  "ta_mobile_app"
    t.integer  "ta_static_web"
    t.integer  "ta_search_engine"
    t.integer  "ta_online_games"
    t.text     "ta_others"
    t.integer  "tp_online_courses"
    t.integer  "tp_edu_games"
    t.integer  "tp_video_tutorial"
    t.integer  "tp_coding_bootcamp"
    t.integer  "tp_day_workshop"
    t.text     "tp_other"
    t.integer  "ts_expensive"
    t.integer  "ts_no_time"
    t.integer  "ts_lack_support"
    t.integer  "ts_low_priority"
    t.text     "ts_other"
    t.integer  "reward_motive"
    t.integer  "rt_beauty_fashion"
    t.integer  "rt_food"
    t.integer  "rt_travel"
    t.integer  "rt_edu_scholarship"
    t.integer  "rt_job"
    t.text     "rt_other"
    t.integer  "wn_dry_and_boring"
    t.integer  "wn_too_difficult"
    t.integer  "wn_not_a_girl_thing"
    t.integer  "wn_just_not_for_me"
    t.string   "public_key"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "public_key"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "location"
    t.integer  "mailchimp_status"
    t.integer  "response_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "mailchimp_status_reason"
  end

end
