# Be sure to restart your server when you modify this file.

# Registers your MailChimp Settings
Gibbon::API.api_key = ENV['mailchimp_api_key']
Gibbon::API.timeout = 15
Gibbon::API.throws_exceptions = false