class SendWelcomeEmailJob < ActiveJob::Base
  queue_as :default

  require 'mandrill'

  MAILCHIMP_STATUS = {
    'registered' => 0
    'sent' => 1,
    'queued' => 1,
    'unsubscribed' => 2
    'rejected' => 10,
    'invalid'=> 11
  }

  def perform(user)
    begin
      md = Mandrill::API.new(ENV['mandrill_api_key'])
      template_name = ENV['mandrill_welcome_template_name']
      template_content = []
      message = {
        'to' => [{'email' => user.email, 'name' => user.first_name}],
        'subject' =>'Thank you for submitting!',
        'merge_vars' => [{'rcpt' => user.email, "vars"=>[
          {'name' => 'fname', 'content' => user.first_name},
          {'name' => 'lname', 'content' => user.last_name},
          {'name' => 'email', 'content' => user.email},
          # TODO: public key will be short link (7digits)
          {'name' => 'unique_link', 'content' => "https://onesmallstep.io/#{user.public_key}"}
        ]}],
        'tags' => ['welcome-email'],
        'important' => true,
      }
      result = md.messages.send_template(template_name, template_content, message)

      user.mailchimp_status = MAILCHIMP_STATUS[result['status']]
      if result['status'] == 'rejected'
        user.mailchimp_status_reason = result['reject_reason']
      end
      user.save
    rescue Mandrill::Error => e
      puts "A mandrill error occurred: #{e.class} - #{e.message}"
    end
  end

end
