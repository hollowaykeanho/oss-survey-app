class ResponsesController < ApplicationController

  def new
    @user = User.new
    @response = Response.new
  end

  def create
    # params[:response][:public_key] = params[:id] if params[:id]
    response = Response.create(responses_params)
    if response.nil?
      redirect_to root_path
    end
    session[:response_id] = response.id
    redirect_to root_path
  end

  private
  def responses_params
    params.require(:response).permit(:location, :age, :tech_interest, :tech_level, :ta_web_app, :ta_mobile_app, :ta_static_web, :ta_search_engine,
                   :ta_online_games, :ta_others, :tp_online_courses, :tp_edu_games, :tp_video_tutorial, :tp_coding_bootcamp, :tp_day_workshop,
                   :tp_other, :ts_expensive, :ts_no_time, :ts_lack_support, :ts_low_priority, :ts_other, :reward_motive, :rt_beauty_fashion,
                   :rt_food, :rt_travel, :rt_edu_scholarship, :rt_job, :rt_other, :wn_dry_and_boring, :wn_too_difficult, :wn_not_a_girl_thing,
                   :wn_just_not_for_me, :public_key)
  end
end
