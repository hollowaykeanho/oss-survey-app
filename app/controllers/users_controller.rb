class UsersController < ApplicationController

  def create
    user = User.create(users_params)
    redirect_to root_path
  end

  private
  def users_params
     params.require(:user).permit(:public_key, :first_name, :last_name, :email, :location, :response_id)
  end
end
